package edu.ntnu.stud;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HandTest {
  private Hand hand;

  @BeforeEach
  void setUp() {
    hand = new Hand();
  }

  @Test
  void addCard() {
    PlayingCard card = new PlayingCard('H', 5);
    hand.addCard(card);
    assertEquals(1, hand.size());
    assertTrue(hand.toString().contains(card.getAsString()));
  }

  @Test
  void clear() {
    PlayingCard card = new PlayingCard('H', 5);
    hand.addCard(card);
    hand.clear();
    assertEquals(0, hand.size());
    assertTrue(hand.toString().isEmpty());
  }

  @Test
  void size() {
    assertEquals(0, hand.size());
    PlayingCard card1 = new PlayingCard('H', 5);
    PlayingCard card2 = new PlayingCard('D', 8);
    hand.addCard(card1);
    hand.addCard(card2);
    assertEquals(2, hand.size());
  }

  @Test
  void hasFlush() {
    // Create a flush hand
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('H', 3));
    hand.addCard(new PlayingCard('H', 4));
    hand.addCard(new PlayingCard('H', 5));
    hand.addCard(new PlayingCard('H', 6));
    assertTrue(hand.hasFlush());

    // Create a non-flush hand
    hand.clear();
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('H', 3));
    hand.addCard(new PlayingCard('D', 4));
    hand.addCard(new PlayingCard('H', 5));
    hand.addCard(new PlayingCard('H', 6));
    assertFalse(hand.hasFlush());
  }

  @Test
  void sumOfFaces() {
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('D', 3));
    hand.addCard(new PlayingCard('C', 4));
    hand.addCard(new PlayingCard('S', 5));
    hand.addCard(new PlayingCard('H', 6));
    assertEquals((2 + 3 + 4 + 5 + 6), hand.sumOfFaces());
  }

  @Test
  void heartsAsString() {
    hand.addCard(new PlayingCard('H', 2));
    hand.addCard(new PlayingCard('D', 3));
    hand.addCard(new PlayingCard('C', 4));
    hand.addCard(new PlayingCard('S', 5));
    hand.addCard(new PlayingCard('H', 6));
    assertEquals("H2 H6", hand.heartsAsString());
  }

  @Test
  void hasSpadeQueen() {
    hand.addCard(new PlayingCard('S', 12)); // Queen of Spades
    assertTrue(hand.hasSpadeQueen());

    hand.clear();
    hand.addCard(new PlayingCard('S', 11)); // Jack of Spades
    assertFalse(hand.hasSpadeQueen());
  }
}
