package edu.ntnu.stud;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

/**
 * Represents a deck of cards. Each deck of cards has 52 cards,
 * where each card is of the type {@link PlayingCard}.
 *
 * @author matteus
 * @version 2024-03-17
 */
public class DeckOfCards {
  private final PlayingCard[] cards;
  private final char[] suit = {'S', 'H', 'D', 'C'};
  private final Set<PlayingCard> hand;


  /**
   * Creates an instance of a DeckOfCards.
   * The deck is created with 52 cards, with 13 cards of each suit.
   */
  public DeckOfCards() {
    cards = new PlayingCard[52];
    int i = 0;

    for (char suit : suit) {
      for (int face = 1; face <= 13; face++) {
        cards[i++] = new PlayingCard(suit, face);
      }
    }
    hand = new HashSet<>();
  }

  /**
   * Method to deal a hand of cards.
   *
   * @param n Integer number of cards to deal.
   * @return HashSet containing {@link PlayingCard}.
   */
  public Hand dealHand(int n) {
    Random random = new Random();
    Hand hand = new Hand();
    hand.clear();

    while (hand.size() < n) {
      int randomCard = random.nextInt(52);
      hand.addCard(cards[randomCard]);
    }
    return hand;
  }

  /**
   * Method to deal a single card.
   *
   * @return {@link PlayingCard}.
   */
  public PlayingCard dealCard() {
    Random random = new Random();
    int randomCard = random.nextInt(52);
    return cards[randomCard];

  }
}
