package edu.ntnu.stud;

import java.util.stream.Stream;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * Represents the GUI of the application.
 *
 * @author matteus
 * @version 2024-03-19
 */
public class CardGameGUI extends Application {
  private final DeckOfCards deck = new DeckOfCards();
  private final Hand hand = new Hand();

  private final TextField numCardsField = new TextField();
  private final Button dealButton = new Button("Deal Cards");
  private final Button checkButton = new Button("Check Cards");

  private final Label handLabel = new Label();
  private final Label sumLabelRes = new Label();
  private final Label heartLabelRes = new Label();
  private final Label queenLabelRes = new Label();
  private final Label flushLabelRes = new Label();

  @Override
  public void start(Stage primaryStage) {
    dealButton.setOnAction(e -> dealHand());
    checkButton.setOnAction(e -> checkHand());

    //Labels
    Label sumLabel = new Label("Sum of faces: ");
    Label heartLabel = new Label("Hearts in hand: ");
    Label queenLabel = new Label("Is the Queen of Spades present? ");
    Label flushLabel = new Label("Is there a flush? ");

    //Boxes
    VBox numCardsBox = new VBox(new Label("Number of cards: "), numCardsField);
    HBox buttonsBox = new HBox(dealButton, handLabel);
    HBox checkBox = new HBox(checkButton);
    HBox box1 = new HBox(sumLabel, sumLabelRes);
    HBox box2 = new HBox(heartLabel, heartLabelRes);
    HBox box3 = new HBox(queenLabel, queenLabelRes);
    HBox box4 = new HBox(flushLabel, flushLabelRes);

    VBox resultBox = new VBox(box1, box2, box3, box4);

    //Spacing
    numCardsBox.setSpacing(5);
    buttonsBox.setSpacing(10);
    checkBox.setSpacing(10);
    resultBox.setSpacing(5);
    VBox.setMargin(checkBox, new Insets(10, 0, 0, 0));

    //Root container
    VBox root = new VBox(numCardsBox, buttonsBox, checkBox, resultBox);
    root.setSpacing(15); //Distance from each other
    root.setPadding(new Insets(15)); //Distance from the left

    //Creating the scene and setting up stage
    Scene scene = new Scene(root, 300, 300);
    primaryStage.setScene(scene);
    primaryStage.setTitle("CardGame Application");
    primaryStage.show();
  }

  private void dealHand() {
    hand.clear();
    int numCards = Integer.parseInt(numCardsField.getText());
    Stream.generate(deck::dealCard)
        .distinct()
        .limit(numCards)
        .forEach(hand::addCard);

    handLabel.setText(hand.toString());
  }

  private void checkHand() {
    sumLabelRes.setText(Integer.toString(sumOfFaces()));
    heartLabelRes.setText(hearts());
    queenLabelRes.setText(hasQueenOfSpades());
    flushLabelRes.setText(hasFlush());
  }

  private int sumOfFaces() {
    return hand.sumOfFaces();
  }

  private String hearts() {
    return hand.heartsAsString();
  }

  private String hasQueenOfSpades() {
    if (hand.hasSpadeQueen()) {
      return "Yes";
    } else {
      return "No";
    }
  }

  private String hasFlush() {
    if (hand.hasFlush()) {
      return "Yes";
    } else {
      return "No";
    }
  }

  public static void main(String[] args) {
    launch(args);
  }
}
