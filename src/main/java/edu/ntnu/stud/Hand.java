package edu.ntnu.stud;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Class to represent the cards in the "hand".
 */
public class Hand {
  private final Set<PlayingCard> cards;

  public Hand() {
    cards = new HashSet<>();
  }

  public void addCard(PlayingCard card) {
    cards.add(card);
  }

  public void clear() {
    cards.clear();
  }

  public int size() {
    return cards.size();
  }

  /**
   * Method to check for flush.
   * Checks whether at least 5 cards in the deck has the same suit.
   *
   * @return boolean whether flush is present or not
   */
  public boolean hasFlush() {
    Set<Character> suits = cards.stream()
        .map(PlayingCard::getSuit)
        .collect(Collectors.toSet());

    return suits.stream().anyMatch(suit -> cards.stream()
            .filter(card -> card.getSuit() == suit).count() >= 5);
  }

  /**
   * Method to find sum of all cards in hand.
   *
   * @return Integer sum of faces.
   */
  public int sumOfFaces() {
    return (cards.stream()
        .mapToInt(PlayingCard::getFace)
        .sum());
  }

  /**
   * Method to find all hearts (as a string).
   *
   * @return String all cards in the hand.
   */
  public String heartsAsString() {
    Set<String> hearts = cards.stream()
        .filter(card -> card.getSuit() == 'H')
        .map(PlayingCard::getAsString)
        .collect(Collectors.toSet());

    return hearts.isEmpty() ? "No Hearts" : String.join(" ", hearts);
  }

  /**
   * Method to check if the card "Queen of spades" (S12) is in the hand.
   *
   * @return boolean True if yes, False if no.
   */
  public boolean hasSpadeQueen() {
    return cards.stream()
        .anyMatch(card -> card.getSuit() == 'S' && card.getFace() == 12);
  }

  /**
   * To string to return hand as a text-string.
   *
   * @return String of cards at hand.
   */
  @Override
  public String toString() {
    return cards.stream()
        .map(PlayingCard::getAsString)
        .collect(Collectors.joining(", "));
  }
}
